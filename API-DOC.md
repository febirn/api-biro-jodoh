#### API Documentation

#### Authorization User Login
check user login or not
#### Parameters
#### Headers
Name        | Type Data | Required | Example
----------- | --------- | -------- | ----------
token       | string    | required | 0fa4e20d25837c596af82b6c07e8b2fa
#### Sample Error
if token not set
````json
{
  "status": 401,
  "message": "Not Authorized"
}

#### Register
#### 1. `POST` Register User
used to register new user
#### 1.2. Resource URL
*URL :* {environment}/public/register
#### 1.3. Sample Error
````json
{
  "status": 400,
  "message": "Errors",
  "data": {
    "name": [
      "Name is required"
    ],
    "username": [
      "Username is required",
      "Username must be at least 6 characters long"
    ],
    "email": [
      "Email is required",
      "Email is not a valid email address"
    ],
    "password": [
      "Password is required",
      "Password must be at least 6 characters long"
    ],
    "gender": [
      "Gender is required"
    ],
    "phone_number": [
      "Phone Number is required",
      "Phone Number must be numeric",
      "Phone Number must not exceed 12 characters"
    ]
  }
}
#### 1.4. Sample Request
````json
{
    "profession_id":1,
    "name":"Febi",
    "username":"febirn",
    "password":"febirn",
    "email":"febirn@nmail.com",
    "gender":"Laki-Laki",
    "phone_number":"085635647892"
}
#### 1.5. Sample Response
{
  "status": 201,
  "message": "Register Success",
  "data": {
    "id": "1",
    "profession_id": "1",
    "username": "febirn",
    "password": "$2y$10$8DrCRWOTaujEDzn5.R9Tye/caqeAIdd6xj3.wewREYok2LZAqSrNC",
    "email": "febirn@nmail.com",
    "name": "Febi",
    "gender": "Laki-Laki",
    "date_birth": null,
    "age": null,
    "marital_status": null,
    "address": null,
    "phone_number": "085635647892",
    "image": null,
    "create_at": "2017-04-19 10:24:37",
    "update_at": "2017-04-19 10:24:37",
    "deleted": "0"
  }
}

#### Login
#### 2. `POST` Login User
used to user login
#### 2.1. Resource URL
*URL :* {environment}/public/login
#### 2.2. Sample Error
if user no registered
````json
{
  "status": 401,
  "message": "Error",
  "data": "Username Not Registered"
}

if wrong passwod
{
  "status": 401,
  "message": "Error",
  "data": "Wrong Password"
}
#### 2.3. Sample Request
````json
{
    "username":"febirn",
    "password":"febirn"
}
#### 2.4 Sample Response
````json
{
  "status": 200,
  "message": "Login Success",
  "data": {
    "id": "1",
    "profession_id": "1",
    "username": "febirn",
    "password": "$2y$10$8DrCRWOTaujEDzn5.R9Tye/caqeAIdd6xj3.wewREYok2LZAqSrNC",
    "email": "febirn@nmail.com",
    "name": "Febi",
    "gender": "Laki-Laki",
    "date_birth": null,
    "age": null,
    "marital_status": null,
    "address": null,
    "phone_number": "085635647892",
    "image": null,
    "create_at": "2017-04-19 10:24:37",
    "update_at": "2017-04-19 10:24:37",
    "deleted": "0"
  },
  "meta": {
    "key": {
      "user_id": "1",
      "token": "1af98299eced9bd2c6964a191a4c6c10",
      "login_at": "2017-04-19 13:55:11",
      "expired_date": "2017-04-19 16:55:11"
    }
  }
}

#### Home 
#### 3. `GET` Get HomePage
Used to get homepage.
#### 3.1. Resource URL
*URL :* {environment}/public
#### 3.2. Sample Response
````json
{
  "status": 200,
  "message": "Success Get Home",
  "data": "Hello Word"
}

#### Users
#### 4. `GET` Get All Users
used to get all users
#### 4.1. Resource URL
*URL :* {environment}/public/users
#### 4.2. Sample Error
if data empty
````json
{
  "status": 404,
  "message": "Data Not Found",
  "data": null
}
#### 4.3 Sample Response
````json
{
  "status": 200,
  "message": "Success Get All Data User",
  "data": [
    {
      "id": "2",
      "profession_id": "1",
      "username": "sariayu",
      "password": "$2y$10$3yv6pbn.3colcNNVLXsZLu0GjlIflF1InlAYnxy/G3mP4mI5bnVBe",
      "email": "sariayu@nmail.com",
      "name": "Sari Ayu",
      "gender": "Perempuan",
      "date_birth": null,
      "age": null,
      "marital_status": null,
      "address": null,
      "phone_number": "085635647892",
      "image": null,
      "create_at": "2017-04-19 13:57:54",
      "update_at": "2017-04-19 13:57:54",
      "deleted": "0"
    },
    {
      "id": "3",
      "profession_id": "1",
      "username": "ayusari",
      "password": "$2y$10$x/LlqLSq5suEG1ng.FiNAOKLWg5ycvBmuoiv.0FNEsUqQyn3DHwYG",
      "email": "ayusari@nmail.com",
      "name": "Ayu",
      "gender": "Perempuan",
      "date_birth": null,
      "age": null,
      "marital_status": null,
      "address": null,
      "phone_number": "085635647892",
      "image": null,
      "create_at": "2017-04-19 13:58:16",
      "update_at": "2017-04-19 13:58:16",
      "deleted": "0"
    }
  ]
}

#### 5. `GET` Get Users By Id
used to get user by id
#### 5.1. Resource URL
*URL :* {environment}/public/users/2
#### 5.2. Sample Error
if data empty
````json
{
  "status": 404,
  "message": "Data Not Found",
  "data": null
}
#### 5.3 Sample Response
````json
{
  "status": 200,
  "message": "Success Get User By Id",
  "data": {
    "id": "2",
    "profession_id": "1",
    "username": "sariayu",
    "password": "$2y$10$3yv6pbn.3colcNNVLXsZLu0GjlIflF1InlAYnxy/G3mP4mI5bnVBe",
    "email": "sariayu@nmail.com",
    "name": "Sari Ayu",
    "gender": "Perempuan",
    "date_birth": null,
    "age": null,
    "marital_status": null,
    "address": null,
    "phone_number": "085635647892",
    "image": null,
    "create_at": "2017-04-19 13:57:54",
    "update_at": "2017-04-19 13:57:54",
    "deleted": "0"
}

#### Message
#### 6. `POST` Add Message
used to Add Message
#### 6.1. Resource URL
*URL :* {environment}/public/message/2
#### 6.2. Sample Request
````json
{
    "message":"test 2"
}
#### 6.3. Sample Response
````json
{
  "status": 201,
  "message": "Success",
  "data": {
    "sender_id": "1",
    "receiver_id": "2",
    "message_id": "1",
    "message": "test 2"
  }
}

#### 7. `GET` Message By ID
used to get Message By Id
#### 7.1. Resource URL
*URL :* {environment}/public/message/3
#### 7.3. Sample Response
````json
{
  "status": 200,
  "message": "Succes",
  "data": [
    {
      "id": "2",
      "sender_id": "1",
      "receiver_id": "3",
      "message_id": "2",
      "message": "test 3",
      "create_at": "2017-04-20 06:56:33"
    },
    {
      "id": "3",
      "sender_id": "3",
      "receiver_id": "1",
      "message_id": "3",
      "message": "test user 1",
      "create_at": "2017-04-20 07:04:37"
    }
  ]
}

#### 8. `GET` Get All Message
used to get all message
#### 8.1. Resource URL
*URL :* {environment}/public/message
#### 8.2. Sample Error
if data empty
{
  "status": 404,
  "message": "Data Not Found",
  "data": null
}
#### 8.3 Samle Response
````json
{
  "status": 200,
  "message": "Get All Messages",
  "data": [
    {
      "message_id": "1",
      "sender_name": "Febi",
      "receiver_name": "Sari Ayu"
    },
    {
      "message_id": "2",
      "sender_name": "Febi",
      "receiver_name": "Ayu"
    }
  ]
}

#### 9. `GET` Users Logout
used to logout user
#### 9.1. Resource URL
*URL :* {environment}/public/logout
#### 9.2. Sample Response
````json
{
  "status": 200,
  "message": "Logout Success",
  "data": null
}
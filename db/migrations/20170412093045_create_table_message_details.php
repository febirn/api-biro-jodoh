<?php

use Phinx\Migration\AbstractMigration;

class CreateTableMessageDetails extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $messageDetail = $this->table('message_detail');
        $messageDetail->addColumn('sender_id', 'integer')
                      ->addColumn('receiver_id', 'integer')
                      ->addColumn('message_id', 'integer')
                      ->addColumn('message', 'text')
                      ->addColumn('create_at', 'timestamp', ['default' => 'CURRENT_TIMESTAMP'])
                      ->addForeignKey('sender_id', 'users', 'id', ['delete' => 'CASCADE', 'update' => 'NO_ACTION'])
                      ->addForeignKey('receiver_id', 'users', 'id', ['delete' => 'CASCADE', 'update' => 'NO_ACTION'])
                      ->addForeignKey('message_id', 'message', 'id', ['delete' => 'CASCADE', 'update' => 'NO_ACTION'])
                      ->create();
    }
}

<?php

use Phinx\Migration\AbstractMigration;

class CreateTableUsers extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $user = $this->table('users');
        $user->addColumn('profession_id', 'integer', ['null' => true])
             ->addColumn('username', 'string')
             ->addColumn('password', 'string')
             ->addColumn('email', 'string')
             ->addColumn('name', 'string')
             ->addColumn('gender', 'string')
             ->addColumn('date_birth', 'date', ['null' => true])
             ->addColumn('age', 'integer', ['null' => true])
             ->addColumn('marital_status', 'integer', ['null' => true])
             ->addColumn('address', 'text', ['null' => true])
             ->addColumn('phone_number', 'string')
             ->addColumn('image', 'string', ['null' => true])
             ->addColumn('create_at', 'timestamp', ['default' => 'CURRENT_TIMESTAMP'])
             ->addColumn('update_at', 'timestamp', ['update' => 'CURRENT_TIMESTAMP', 'default' => 'CURRENT_TIMESTAMP'])
             ->addColumn('deleted', 'integer', ['default' => 0])
             ->addForeignKey('profession_id', 'profession', 'id', ['delete' => 'CASCADE', 'update' => 'NO_ACTION'])
             ->create();
    }
}

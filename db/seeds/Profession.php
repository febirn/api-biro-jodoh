<?php

use Phinx\Seed\AbstractSeed;

class Profession extends AbstractSeed
{
    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeders is available here:
     * http://docs.phinx.org/en/latest/seeding.html
     */
    public function run()
    {
        $data[] = ['name' => 'Karyawan BUMN' ];
        $data[] = ['name' => 'Karyawan Swasta' ];
        $data[] = ['name' => 'Programmer' ];
        $data[] = ['name' => 'Designer' ];

        $this->insert('profession', $data);
    }
}

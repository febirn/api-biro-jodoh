<?php

use Phinx\Seed\AbstractSeed;

class Users extends AbstractSeed
{
    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeders is available here:
     * http://docs.phinx.org/en/latest/seeding.html
     */
    public function run()
    {
        $faker = Faker\Factory::create();

        $id = array_rand([1,2,3,4], 1);
        $gender = array_rand([0,1], 1);

        $data = [];
        for ($i = 0; $i < 2; $i++) { 
            $data[] = [
                'profession_id' => $id,
                'username'      => $faker->userName,
                'password'      => $faker->password,
                'email'         => $faker->email,
                'first_name'    => $faker->firstName,
                'last_name'     => $faker->lastName,
                'gender'        => $gender,
                'date_birth'    => date($format = 'Y-m-d'),
                'age'           => 20,
                'marital_status'    => $gender,
                'address'       => $faker->streetAddress,
                'phone_number'  => $faker->e164PhoneNumber,
            ];
        }

        $this->insert('users', $data);
    }
}

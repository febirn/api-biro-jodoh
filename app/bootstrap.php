<?php

use Slim\App;

require __DIR__ . '/../vendor/autoload.php';

$app = new App(require __DIR__ . '/settings.php');

require __DIR__ . '/container.php';
require __DIR__ . '/routes.php';

?>
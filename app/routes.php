<?php

$app->post('/register', 'App\Controllers\UserController:register');

$app->post('/login', 'App\Controllers\UserController:login');

$app->group('', function() use ($app, $container) {
    $app->group('', function() use ($app, $container) {   
        $app->get('/', 'App\Controllers\HomeController:index');

        $app->get('/users', 'App\Controllers\UserController:index');

        $app->get('/users/{id}', 'App\Controllers\UserController:getById');

        $app->get('/logout', 'App\Controllers\UserController:logout');

        $app->get('/message', 'App\Controllers\MessageController:index');

        $app->get('/message/{id}', 'App\Controllers\MessageController:getMessage');
        
        $app->post('/message/{id}', 'App\Controllers\MessageController:addMessage');
    })->add(new \App\Middleware\AdminMiddleware($container));
})->add(new \App\Middleware\AuthToken($container));

?>
<?php

namespace App\Models;

abstract class BaseModel
{
    protected $db;
    protected $table;
    protected $columnTable;

    public function __construct($db)
    {
        return $this->db = $db;
    }

    public function getAll()
    {
        $qb = $this->db->createQueryBuilder();

        $qb->select('*')
           ->from($this->table);

        $result = $qb->execute();

        return $result->fetchAll();
    }

    public function create(array $data)
    {
        $qb = $this->db->createQueryBuilder();

        $valuesColumn = [];
        $valuesData = [];

        foreach ($data as $dataKey => $dataValue) {
            $valuesColumn[$dataKey] = ':' . $dataKey;
            $valuesData[$dataKey] = $dataValue;
        }

        $qb->insert($this->table)
            ->setParameters($valuesData)
            ->values($valuesColumn)
            ->execute();

        return $this->db->lastInsertId();
    }

    public function update($data, $column, $value)
    {
        $qb = $this->db->createQueryBuilder();

        $valuesColumn = [];
        $valuesData = [];

        $qb->update($this->table);

        foreach ($data as $dataKey => $dataValue) {
            $valuesColumn[$dataKey] = ':' . $dataKey;
            $valuesData[$dataKey] = $dataValue;

            $qb->set($dataKey, $valuesColumn[$dataKey]);
        }

        $qb->setParameters($valuesData)
            ->where($column . ' = ' . $value)
            ->execute();
    }

    public function find($column, $value)
    {
        $qb = $this->db->createQueryBuilder();

        $param = ':' . $column;

        $qb->select('*')
           ->from($this->table)
           ->setParameter($param, $value)
           ->where($column . ' = ' . $param);

        $result = $qb->execute();

        return $result->fetch();
    }

    public function search($column, $value)
    {
        $qb = $this->db->createQueryBuilder();

        $param = "'%" . $value . "%'";

        $qb->select('*')
           ->from($this->table)
           ->where($column . ' LIKE ' . $param);

        $result = $qb->execute();

        return $result->fetchAll();
    }

    public function delete($column, $value)
    {
        $qb = $this->db->createQueryBuilder();
        
        $param = ':' . $column;

        $qb->delete($this->table)
           ->setParameter($param, $value)
           ->where($column . ' = ' . $param)
           ->execute();
    }
}


?>
<?php

namespace App\Models\Users;

use App\Models\BaseModel;

class UserToken extends BaseModel
{
    protected $table = 'user_token';

    public function setToken($id)
    {
        $data = [
            'user_id'       =>  $id,
            'token'         =>  md5(openssl_random_pseudo_bytes(12)),
            'login_at'      =>  date('Y-m-d H:i:s'),
            'expired_date'  =>  date('Y-m-d H:i:s', strtotime('+3 hours')),
        ];

        $now = date('Y-m-d H:i:s', strtotime('now'));

        $find = $this->find('user_id', $id);
        
        if ($find || $find['expired_date'] > $now) {
            $this->update($data, 'user_id', $id);
        } else {
            $this->create($data);
        }

        return $data;
    }
}

?>
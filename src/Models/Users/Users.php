<?php

namespace App\Models\Users;

use App\Models\BaseModel;

class Users extends BaseModel
{
    protected $table = 'users';

    public function register($data)
    {
        $data = [
            'profession_id'     =>  $data['profession_id'],
            'name'              =>  $data['name'],
            'username'          =>  $data['username'],
            'email'             =>  $data['email'],
            'password'          =>  password_hash($data['password'], PASSWORD_DEFAULT),
            'gender'            =>  $data['gender'],
            'date_birth'        =>  $data['date_birth'],
            'age'               =>  $data['age'],
            'marital_status'    =>  $data['marital_status'],
            'address'           =>  $data['address'],
            'phone_number'      =>  $data['phone_number'],
            'image'             =>  $data['image'],
        ];

        $this->create($data);

        return $this->db->lastInsertId();
    }

}

?>
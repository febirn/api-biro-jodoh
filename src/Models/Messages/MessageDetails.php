<?php

namespace App\Models\Messages;

use App\Models\BaseModel;

class MessageDetails extends BaseModel
{
    protected $table = 'message_detail';
    protected $tableJoin = ['message', 'users'];

    public function findMessage($column1, $column2, $value1, $value2, $find = null)
    {
        $qb = $this->db->createQueryBuilder();

        $param1 = ':' . $column1;
        $param2 = ':' . $column2;

        $qb->select('*')
           ->from($this->table)
           ->setParameter($param1, $value1)
           ->setParameter($param2, $value2)
           ->where($column1 . ' = ' . $param1 . ' AND ' . $column2 . ' = ' . $param2)
           ->orWhere($column1 . ' = ' . $param2 . ' AND ' . $column2 . ' = ' . $param1);
           
        $result = $qb->execute();

        if ($find == null) {
            $result = $result->fetchAll(); 
        } else {
            $result = $result->fetch();
        }

        return $result;
    }

    public function allMessage($user_id)
    {
        $qb = $this->db->createQueryBuilder();

        $qb->select('DISTINCT md.message_id','us.name as sender_name', 'ur.name as receiver_name')
           ->from($this->table, 'md')
           ->join('md', $this->tableJoin[0], 'm', 'm.id = md.message_id')
           ->join('md', $this->tableJoin[1], 'us', 'us.id = md.sender_id')
           ->join('md', $this->tableJoin[1], 'ur', 'ur.id = md.receiver_id')
           ->where('md.sender_id = ' . $user_id);
        
        $result = $qb->execute();

        return $result->fetchAll();
    }

}

?>
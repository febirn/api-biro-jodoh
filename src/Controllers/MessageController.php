<?php

namespace App\Controllers;

use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

class MessageController extends BaseController
{
    public function index(Request $request, Response $response)
    {
        $findUserToken = $this->getUserToken();

        $message = new \App\Models\Messages\Message($this->db);
        $messageDetail = new \App\Models\Messages\MessageDetails($this->db);

        $findMessageDetail = $messageDetail->allMessage($findUserToken['user_id']);

        if (!$findMessageDetail) {
            $data = $this->responseJson(404, "Data Not Found");
        } else {
            $data = $this->responseJson(200, 'Get All Messages', $findMessageDetail);
        }

        return $data;
    }

    public function addMessage(Request $request, Response $response, $args)
    {        
        $findUserToken = $this->getUserToken();

        $message = new \App\Models\Messages\Message($this->db);
        $messageDetail = new \App\Models\Messages\MessageDetails($this->db);

        $findMessage = $messageDetail->findMessage('sender_id', 'receiver_id', $findUserToken['user_id'], $args['id'], 1);

        $dataMessage = [
            'create_at' =>  date('Y-m-d H:i:s', strtotime('now')),
        ];
        
        if (!$find) {
            $messageId = $message->create($dataMessage);
        } else {
            $messageId = $find['message_id'];
        }

        $data = [
            'sender_id'     =>  $findUserToken['user_id'],
            'receiver_id'   =>  $args['id'],
            'message_id'    =>  $messageId,
            'message'       =>  $request->getParsedBody()['message'],
        ];

        $messageDetail->create($data);

        return $this->responseJson(201, "Success", $data);
    }

    public function getMessage(Request $request, Response $response, $args)
    {
        $findUserToken = $this->getUserToken();

        $messageDetail = new \App\Models\Messages\MessageDetails($this->db);

        $findMessage = $messageDetail->findMessage('sender_id', 'receiver_id', $findUserToken['user_id'], $args['id']);

        if (!$findMessage) {
            $data = $this->responseJson(404, "Data Not Found");
        } else {
            $data = $this->responseJson(200, "Succes", $findMessage);
        }

        return $data;
    }
}

?>
<?php

namespace App\Controllers;

use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

class UserController extends BaseController
{
    public function index(Request $request, Response $response)
    {
        $findUserToken = $this->getUserToken();

        $user = new \App\Models\Users\Users($this->db);
    
        $userData = $user->find('id', $findUserToken['user_id']);

        if ($userData['gender'] == 'Laki-Laki') {
            $userGender = $user->search('gender', 'Perempuan');
        } elseif ($userData['gender'] == "Perempuan") {
            $userGender = $user->search('gender', 'Laki-Laki');
        }

        if (!$userGender) {
            $data = $this->responseJson(404, "Data Not Found");
        } else {
            $data = $this->responseJson(200, "Success Get All Data User", $userGender);
        }

        return $data;
    }

    public function getById(Request $request, Response $response, $args)
    {
        $user = new \App\Models\Users\Users($this->db);

        $userData = $user->find('id', $args['id']);

        if (!$userData) {
            $data = $this->responseJson(400, "Data Not Found");
        } else {
            $data = $this->responseJson(200, "Success Get User By Id", $userData);
        }

        return $data;
    }
    
    public function register(Request $request, Response $response)
    {
        $user = new \App\Models\Users\Users($this->db);
        
        $rule = [
            'required' => [
                ['name'],
                ['username'],
                ['email'],
                ['password'],
                ['gender'],
                ['phone_number'],
            ],
            'email' => [
                ['email'],
            ],
            'numeric' => [
                ['phone_number'],
            ],
            'lengthMin' => [
                ['username', 6],
                ['password', 6],
            ],
            'lengthMax' => [
                ['phone_number', 12],
            ],
        ];

        $this->validator->rules($rule);

        if ($this->validator->validate()) {
            
            $addUser = $user->register($request->getParsedBody());

            $findData = $user->find('id', $addUser);

            $data = $this->responseJson(201, "Register Success", $findData);
        } else {
            $data = $this->responseJson(400, "Errors", $this->validator->errors());
        }

        return $data;
    }

    public function login(Request $request, Response $response)
    {
        $user = new \App\Models\Users\Users($this->db);

        $login = $user->find('username', $request->getParsedBody()['username']);

        if (empty($login)) {
            $data = $this->responseJson(401, "Error", "Username Not Registered");
        } else {
            $check = password_verify($request->getParsedBody()['password'], $login['password']);

            if ($check) {
                $token = new \App\Models\Users\UserToken($this->db);
                
                $getToken = $token->setToken($login['id']);

                $key = [
                    'key'   => $getToken,
                ];

                $data = $this->responseJson(200, "Login Success", $login, $key);
            } else {
                $data = $this->responseJson(401, "Error", "Wrong Password");
            }
        }

        return $data;
    }

    public function logout(Request $request, Response $response)
    {        
        $findUserToken = $this->getUserToken();
        
        $userToken = new \App\Models\Users\UserToken($this->db);

        $userToken->delete('user_id', $findUserToken['user_id']);

        return $this->responseJson(200, "Logout Success");
    }
}

?>
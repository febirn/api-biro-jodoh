<?php

namespace App\Controllers;

abstract class BaseController
{
    protected $container;

    public function __construct($container)
    {
        return $this->container = $container;
    }

    public function __get($property)
    {
        return $this->container->{$property};
    }

    public function getUserToken()
    {
        $token = $this->request->getHeader('Authorization')[0];

        $userToken = new \App\Models\Users\UserToken($this->db);
        $findUser = $userToken->find('token', $token);

        return $findUser;
    }

    public function responseJson($status, $message, $data = null, $meta = null)
    {
        $response = [
            'status'    =>  $status,
            'message'   =>  $message,
            'data'      =>  $data,
            'meta'      =>  $meta,
        ];

        if ($data == null || $meta == null) {
            array_pop($response);
        };

        return $this->response->withHeader('Content-type', 'application/json')
            ->withJson($response, $status);
    }
}

?>
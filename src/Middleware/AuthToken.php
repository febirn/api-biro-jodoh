<?php

namespace App\Middleware;

use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

class AuthToken extends BaseMiddleware
{
    public function __invoke(Request $request, Response $response, $next)
    {
        $token = $request->getHeader('Authorization')[0];

        $userToken = new \App\Models\Users\UserToken($this->container->db);

        $findUser = $userToken->find('token', $token);

        $now = date('Y-m-d H:i:s');

        if (!$findUser || $findUser['expired_date'] < $now) {
            $data['status'] = 401;
            $data['message'] = "Not Authorized";

            $userToken->delete('token', $token);

            return $response->withHeader('Content-type', 'application/json')
                ->withJson($data, $data['status']);
        }

        $response = $next($request, $response);
        
        $addTime['expired_date'] = date('Y-m-d H:i:s', strtotime($findUser['expired_date']. '+3 hours'));

        $userToken->update($addTime, 'user_id', $findUser['user_id']);

        return $response;
    }
}

?>
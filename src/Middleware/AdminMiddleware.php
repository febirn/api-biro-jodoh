<?php 

namespace App\Middleware;

use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

class AdminMiddleware extends BaseMiddleware
{
    public function __invoke($request, $response, $next)
    {
        $token = $request->getHeader('Authorization')['0'];

        $userToken = new \App\Models\Users\UserToken($this->container->db);
        $findToken = $userToken->find('token', $token);

        $user = new \App\Models\Users\Users($this->container->db);
        $findUser = $user->find('id', $findToken['user_id']);

        if (!$findUser) {
            $data['status'] = 401;
            $data['message'] = 'You Are Not Login';
            return $response->withHeader('Content-type', 'application/json')->withJson($data, $data['status']);
        }

        $response = $next($request, $response);

        return $response;
    }
}

?>
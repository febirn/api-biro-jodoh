Feature: register new user
    
@mink:selenium2
    Scenario: register new user
    Given I some data
    | name         | username | password |     email     | gender    | phone_number |
    | Rian Sevan   | rianevan | rianevan | rian@mail.com | laki-laki | 087087849827 |
    And I post request url "/public/register"
    Then I get response code 201
